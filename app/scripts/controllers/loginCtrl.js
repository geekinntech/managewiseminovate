'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:PagesLoginCtrl
 * @description
 * # PagesLoginCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('LoginCtrl', function($scope, $state, $location, $cookies, Global, AuthService) {

    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;


    $scope.login = function() {
      $scope.data = {};
      $scope.data.user = $scope.user;
      AuthService.login($scope.subdomain, $scope.domain, $scope.data).success(function(data) {
      	debugger;
        $cookies.email = data.email;
        $cookies.auth_token = data.auth_token;
        $cookies.first_name = data.first_name;
        $cookies.last_name = data.last_name;
        $cookies.log_flag = data.log_flag;
        $cookies.role_name = data.role_name;
        $cookies.permission_allowed = JSON.stringify(data.permission_allowed);

        if (data.branch_id != null) {

          $cookies.branch_id = data.branch_id;
        };

        $location.path('/app/dashboard');
      }).error(function(data) {

      });

    };
  });
