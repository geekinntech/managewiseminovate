'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormsCommonCtrl
 * @description
 * # FormsCommonCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('UsersCtrl', function($scope) {
    $scope.page = {
      title: 'Users',
      subtitle: 'list of all the users'
    };
  })
  .controller('UserDatatableCtrl', ['$scope', '$location', '$cookies', 'Global',
    'DTOptionsBuilder',
    'DTColumnBuilder',
    '$resource',
    'UserService',
    'ngTableParams',
    function($scope, $location, $cookies, Global, DTOptionsBuilder, DTColumnBuilder, $resource, UserService, ngTableParams) {

      $scope.subdomain = $location.$$host.split('.')[0];
      $scope.domain = Global.rails_domain;

      UserService.tempSearch($scope.domain, $scope.subdomain).success(function(data) {

        $scope.users = data.users;
      }).error(function(data) {
        console.dir(data);
      });

      $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
          foo: 'asc' // initial sorting
        }
      }, {
        // total: $scope.users.length // length of data

      });

    }
  ]);
