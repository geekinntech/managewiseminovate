'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:PagesForgotPasswordCtrl
 * @description
 * # PagesForgotPasswordCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('ForgotPasswordCtrl', function($rootScope, $scope, $location, Global,AuthService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.rails_domain = Global.rails_domain;

    $rootScope.$on('ngDialog.opened', function(e, $dialog) {

      if ($scope.alert) {
        $("#" + $scope.alert.id).children("div.ngdialog-content").addClass("delete");
      }
    });

    $scope.data = {};
    $scope.send_recovery_email = function() {
      // $.fancybox.showLoading();
      // $.fancybox.helpers.overlay.open({
      //   parent: $('body')
      // });
      AuthService.send_recovery_email($scope.subdomain, $scope.rails_domain, $scope.data).success(function(data) {
        // $.fancybox.hideLoading();
        // $.fancybox.helpers.overlay.close();
        alert("Email has been sent");
      }).error(function(data) {
        // $.fancybox.hideLoading();
        // $.fancybox.helpers.overlay.close();
        alert("Email not found in our database");
      });
    }

    $scope.show_alert = function(string) {
      $('.btn').blur();
      // $scope.alert = ngDialog.open({
      //   template: '<div  class="dialog"><div><h3 id="myModalLabel" class="modal-title">Alert </h3></div><div class="modal-body"> <h4>' + string + '</h4></div><div class="modal-footer"><button data-dismiss="modal" ng-click="closeThisDialog()" class="btn btn-success" type="button">OK</button></div></div>',
      //   plain: true,
      //   showClose: false,
      //   scope: $scope,
      //   closeByDocument: false
      // })
    }
  });
