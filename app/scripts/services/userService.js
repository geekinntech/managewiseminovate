angular.module('minovateApp')
  .factory('UserService', ['$q', '$http','$cookies', function($q, $http, $cookies) {

    return {

      tempSearch: function(domain,subdomain) {
        // var deferred = $q.defer();

        return $http({
          method: "GET",
          url: "http://"+subdomain+"."+domain+"/api/members",
          crossDomain: true,
          dataType: "JSONP",
          data: [],
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        });

        // request.success(function(data) {
        //   deferred.resolve(data.users);
        // });
        // request.error(function(reason) {
        //   deferred.reject(reason);
        // });

        // return deferred.promise;
        // return request.users;
      },

    }
  }]);
